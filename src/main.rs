use std::i32;
use std::io;

fn main() {
    // Tells the user what the program is
    println!("Counting digits program");
    // Variables
    let mut x = String::new();
    let mut digits = 0;
    // Grabs user input
    println!("Enter the number");
    io::stdin().read_line(&mut x).ok()
        .expect("Failed to read line");
    // Converting string to integer
    let mut xint: i32 = x.trim().parse().ok()
        .expect("Program only processes numbers, Enter number");

    // Counts the amount of digits
    while xint > 0 {
        xint = xint / 10;
        digits += 1;
    }
    // Prints the digits
    if digits == 1 {
        println!("There is {} digit", digits);
    } else {
        println!("There are {} digits", digits);
    }
}
